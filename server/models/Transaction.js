const mongoose = require('mongoose');

//create new instance of the mongoose.schema. the schema takes an 
//object that shows the shape of your database entries.
var TransactionSchema = new mongoose.Schema({
 data: String,
 time: String,
 wallet: String,
 typeOfOperation: String, // TypeOfOperation
 sum: String,
 categories: String
});

//export our module to use in server.js
module.exports = mongoose.model('transactions', TransactionSchema);
