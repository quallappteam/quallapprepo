//Express

const express = require('express')
const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const config = require('../config/Config');
const mongoose = require('mongoose');

// Set up Mongoose
mongoose.connect(config.db_dev);
mongoose.Promise = global.Promise;

// Cors
var cors = require('cors');
app.use(cors());

app.get('/', (req, res) => {
    res.send('Hello World!')
  });

app.get('/ping', (req, res) => {
    res.send("pong")
});

const Transaction = require("./models/Transaction");

app.post('/api/transactionAdd/:data/:time/:wallet/:typeOfOperation/:sum/:categories', (req, res, next) => {
    
    const transaction = new Transaction()
    
    transaction.data = req.params.data
    transaction.time = req.params.time
    transaction.wallet = req.params.wallet
    transaction.typeOfOperation = req.params.typeOfOperation
    transaction.sum = req.params.sum
    transaction.categories = req.params.categories

    transaction.save()

    res.json(transaction);
  });

app.listen(8000, () => {
console.log('Example app listening on port 8000!')
});
