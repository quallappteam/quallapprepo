//Импорт
import React, {Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import {styles} from '../Style'
import {typeOfOperation} from './TypesOfOperation';
//Объявление переменных
var flagfordisplay=true;
var text = "0";
export var result = 0;
var varOfOperation = 0; // 10 - Сумма, 11 - Вычитание, 12 - Умножение, 13 - Деление
var calculator_obj;
export function settext (newValuetext) {
    text = newValuetext;
    result = newValuetext;
    calculator_obj.forceUpdate();    
}
export default class Calculator extends Component {
    //Функция для выбора подходящей опции OnPress для кнопки калькулятора
    computation(numberOfButton) // 0-9 - Цифры, 10 - Сумма, 11 - Вычитание, 12 - Умножение, 13 - Деление
    {
        if (parseInt(numberOfButton)>9)
        {
           if (varOfOperation==0)
                {
                varOfOperation=numberOfButton;
                this.forceUpdate();
                result=parseInt(text);
                text=0;  
                flagfordisplay=false; 
                }
           else
                {
                if (varOfOperation==10)
                    {
                    result=result+parseInt(text);
                    }
                else if (varOfOperation==11)
                    {
                    result=result-text;
                    }
                else if (varOfOperation==12)
                    {
                    result=result*text;
                    }   
                else if (varOfOperation==13)
                    {
                    result=result/text;
                    }
                else if (varOfOperation==14)
                    {
                    //result=0;
                    }
                varOfOperation=numberOfButton;
                text=0;
                flagfordisplay=false;
                this.forceUpdate();                       
                }    
        }    
        else
            {
            if (text==0)
            {
                text = numberOfButton
                flagfordisplay=true;
                this.forceUpdate();
            }
            else
                {
                    text = text + numberOfButton
                    flagfordisplay=true;
                    this.forceUpdate();
                }
            }
    }
    getTouchableOpacity(number) 
    {
        return(
            <TouchableOpacity 
            style={styles.buttonCalc}
            onPress={() => {this.computation(number.toString())}}>
            <Text>
                {number}                       
            </Text>
            </TouchableOpacity>
        )
    }
    //Условие для выбора, что показывать на дисплее: результат или вводимое число 
    render()
    {
        calculator_obj = this
        const textText = 
        (
            <Text style={styles.calculatorScreen}>
                <center>{text}</center>
            </Text>
            
        )
        const resultText =
        (
            <Text style={styles.calculatorScreen}>
                <center>{result}</center>
            </Text>
        )
        return(
            //Дисплей
            <View>
            {flagfordisplay ? textText : resultText}          
            <View style={styles.buttonCalcWindows}>           
            {this.getTouchableOpacity("1")} 
            {this.getTouchableOpacity("2")} 
            {this.getTouchableOpacity("3")}   
                <TouchableOpacity 
                    style={styles.buttonCalc} 
                    onPress={() => {this.computation("10")}}
                >
                    <Text>
                        +
                    </Text>
                </TouchableOpacity>
                {this.getTouchableOpacity("4")} 
                {this.getTouchableOpacity("5")} 
                {this.getTouchableOpacity("6")} 
                <TouchableOpacity 
                    style={styles.buttonCalc}
                    onPress={() =>{this.computation("11")}}
                >
                    <Text>
                        -
                    </Text>
                </TouchableOpacity>
                {this.getTouchableOpacity("7")} 
                {this.getTouchableOpacity("8")} 
                {this.getTouchableOpacity("9")} 
                <TouchableOpacity 
                    style={styles.buttonCalc} 
                    onPress={() => {this.computation("12")}}
                >
                    <Text>
                        x
                    </Text>
                </TouchableOpacity>
                {this.getTouchableOpacity("0")} 
                <TouchableOpacity 
                    style={styles.buttonCalc}
                    onPress={() => {this.computation("13")}}
                >
                    <Text>
                        /
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonCalc}
                    onPress={() => {this.computation("14")}}
                >
                    <Text>
                        =
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonCalc}
                    onPress={() => {this.computation("15")}}
                >
                    <Text>
                        CE
                    </Text>
                </TouchableOpacity>
                </View>
            </View>
        )
    }
}