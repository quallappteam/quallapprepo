//Импорт
import React, {Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import {styles} from '../Style'
import Calculator, { result } from './Calculator'
import {typeOfOperation} from './TypesOfOperation'
import {wallet} from './Wallets'
import transactionAdd from '../Api/api'
import {settext} from './Calculator'
//Объявление переменных

//
export default class Categories extends Component {
    render()
    {
        return(
            <View style={styles.buttonCalcWindows}>
                <TouchableOpacity 
                    style={styles.buttonMenuCategories}
                    onPress={() =>{
                        transactionAdd('','',wallet,typeOfOperation,result,'Еда')
                        settext(0)
                    }}
                >
                    <Text>
                        Еда
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuCategories}
                    onPress={() =>{
                        transactionAdd('','',wallet,typeOfOperation,result,'Машина')
                        settext(0)
                    }}
                >
                    <Text>
                        Машина
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuCategories}
                    onPress={() =>{
                        transactionAdd('','',wallet,typeOfOperation,result,'Проезд')
                        settext(0)
                    }}
                >
                    <Text>
                        Проезд
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuCategories}
                    onPress={() =>{
                        transactionAdd('','',wallet,typeOfOperation,result,'Развлечения')
                        settext(0)
                    }}
                >
                    <Text>
                        Развлечения
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuCategories}
                    onPress={() =>{
                        transactionAdd('','',wallet,typeOfOperation,result,'Кредит')
                        settext(0)
                    }}
                >
                    <Text>
                        Кредит
                    </Text>
                </TouchableOpacity>  
            </View>
    )
}}