import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {styles}  from '../Style'

export default class Welcome extends Component {
    render() {
      return (
        <View>
        <View style={styles.appHeader}>
        <Text style={styles.appTitle}>
            <q><i>
            QuallApp — твой помощник в эффективном управлении своей жизнью.
            </i></q>
        </Text>
        </View>
        <Text style={styles.appIntro}>
          В первую очередь, научимся управлять деньгами
        </Text>
        </View>
      )
}}