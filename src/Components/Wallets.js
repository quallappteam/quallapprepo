//Импорт
import React, { Component } from 'react'
import { Button, View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { styles } from '../Style'
//Объявление переменных
export var wallet; 
//
export default class Menu extends Component {
    render()
    {
        return(
            <View style={styles.buttonCalcWindows}>
                <TouchableOpacity 
                    style={styles.buttonMenuWallets}
                    onPress={() =>{this.forceUpdate();wallet='Наличные'}}
                >
                    <Text>
                        Наличные
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuWallets}
                    onPress={() =>{this.forceUpdate();wallet='Дебетовая'}}
                >
                    <Text>
                        Дебетовая
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuWallets}
                    onPress={() =>{this.forceUpdate();wallet='Кредитная'}}
                >
                    <Text>
                        Кредитная
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuWallets}
                    onPress={() =>{this.forceUpdate();wallet='Сбербанк'}}
                >
                    <Text>
                        Сбербанк
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonMenuWallets}
                    onPress={() =>{this.forceUpdate();wallet='Клюква'}}
                >
                    <Text>
                        Клюква
                    </Text>
                </TouchableOpacity>  
            </View>
    )
}}