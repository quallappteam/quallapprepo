//Импорт
import React, {Component} from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import {styles} from '../Style'
//Объявление переменных
export var typeOfOperation
//
export default class TypesOfOperations extends Component {
    render()
    {
        return(
            <View style={styles.buttonCalcWindows}>           
                <TouchableOpacity
                    style={styles.buttonIncome}
                    onPress={() => {typeOfOperation='Доход'}}
                >
                    <Text>
                        Доход                        
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonConsumption}
                    onPress={() => {typeOfOperation='Расход'}}
                >
                    <Text>
                        Расход                        
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonIncome}
                    onPress={() => {typeOfOperation='Долг'}}
                >
                    <Text>
                        Долг                        
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    style={styles.buttonConsumption}
                    onPress={() => {typeOfOperation='Перевод'}}
                >
                    <Text>
                        Перевод                        
                    </Text>
                </TouchableOpacity>
        </View>
    )
}}