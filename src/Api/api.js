function transactionAdd (data, time, wallet, typeOfOperation, sum, categories){ 
        // Определение даты
        data = new Date().getDate() + '-' + (parseInt(new Date().getMonth())+1) + '-' + new Date().getFullYear()
        // Определение времени
        time = new Date().getHours() + ':' + new Date().getMinutes()
        // Отправка в БД
        return fetch(`http://localhost:8000/api/transactionAdd/${data}/${time}/${wallet}/${typeOfOperation}/${sum}/${categories}`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Credentials': 'http://localhost:8000'
            }
        }).then((response) => {
            console.log(response)
        }).catch((error) => {
            console.error(error);
        });;
    }
    export default transactionAdd;