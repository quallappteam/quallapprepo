import {StyleSheet} from 'react-native'
export const styles = StyleSheet.create({
    app: {
      flex: 1,
      backgroundColor:'#3c1f4f'
    },
    appHeader: {
      flex: 1,
      backgroundColor: '#222',
      padding: 15,
      justifyContent: 'center',
      alignItems: 'center'
    },
    appTitle: {
      fontSize: 25,
      color: 'white'
    },
    appIntro: {
      flex: 2,
      fontSize: 30,
      textAlign: 'center'
    },
    buttonCalcWindows: 
    {
      height:'100%', 
      marginLeft: '38%',
      width:'24%',
      flexDirection: 'row',
      flexWrap: 'wrap'
    },
    buttonCalc: 
    {
      height:'80px', 
      width:'25%', 
      color:'#df8bd4', 
      backgroundColor:'#643169', 
      fontsize: '30',
      fontWeight: 'bold',
      alignItems:'center', //По горизонтали
      justifyContent: 'center', //По вертикали
      borderLeftWidth: '5%',
      borderLeftColor: 'white',
      borderRightWidth: '5%',
      borderRightColor: 'white',
      borderTopWidth: '5%',
      borderTopColor: 'white',
      borderBottomWidth: '5%',
      borderBottomColor: 'white'
    },
    buttonIncome: 
    {
      height:'40px', 
      width:'50%', 
      color:'#ffffff', 
      backgroundColor:'#34a853', 
      fontSize: '30',
      fontWeight: 'bold',
      alignItems:'center', //По горизонтали
      justifyContent: 'center', //По вертикали
      borderLeftWidth: '5%',
      borderLeftColor: 'white',
      borderRightWidth: '5%',
      borderRightColor: 'white',
      borderTopWidth: '5%',
      borderTopColor: 'white',
      borderBottomWidth: '5%',
      borderBottomColor: 'white' 
    },
    buttonConsumption: 
    {
      height:'40px', 
      width:'50%', 
      color:'#ffffff', 
      backgroundColor:'#ea4335', 
      fontSize: '13px',
      fontWeight: 'bold',
      alignItems:'center', //По горизонтали
      justifyContent: 'center', //По вертикали
      borderLeftWidth: '5%',
      borderLeftColor: 'white',
      borderRightWidth: '5%',
      borderRightColor: 'white',
      borderTopWidth: '5%',
      borderTopColor: 'white',
      borderBottomWidth: '5%',
      borderBottomColor: 'white'
    },
    buttonMenu: 
    {
      height:'40px', 
      width:'20%', 
      color:'#df8bd4', 
      backgroundColor:'#643169', 
      alignItems:'center', //По горизонтали
      justifyContent: 'center', //По вертикали
      fontSize: '30',
      fontWeight: 'bold',
    },
    buttonMenuWallets: 
    {
      height:'40px', 
      width:'20%', 
      color:'#df8bd4', 
      backgroundColor:'#643169', 
      alignItems:'center', //По горизонтали
      justifyContent: 'center', //По вертикали
      fontSize: '30',
      fontWeight: 'bold',
      borderLeftWidth: '5%',
      borderLeftColor: 'white',
      borderRightWidth: '5%',
      borderRightColor: 'white',
      borderTopWidth: '5%',
      borderTopColor: 'white',
      borderBottomWidth: '5%',
      borderBottomColor: 'white'
    },
    buttonMenuCategories: 
    {
      height:'40px', 
      width:'20%', 
      color:'#ef7b00', 
      backgroundColor:'#643169', 
      alignItems:'center', //По горизонтали
      justifyContent: 'center', //По вертикали
      fontSize: '30',
      fontWeight: 'bold',
      borderLeftWidth: '5%',
      borderLeftColor: 'white',
      borderRightWidth: '5%',
      borderRightColor: 'white',
      borderTopWidth: '5%',
      borderTopColor: 'white',
      borderBottomWidth: '5%',
      borderBottomColor: 'white'
    },
    calculatorScreen:
    {
      color: '#ffffff'
    }
  })