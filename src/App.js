import React, {Component} from 'react'
import {View, Text, TextInput, TouchableOpacity} from 'react-native'
import Calculator from './Components/Calculator'
import {styles}  from './Style'
import Menu from './Components/Menu'
import Categories from './Components/Categories'
import Wallets from './Components/Wallets'
import TypesOfOperations from './Components/TypesOfOperation'
import Welcome from './Components/Welcome'

export default class App extends Component {
  render() {
    return (
        <View style={styles.app}>
          <Menu />
          <Welcome />
          <Wallets />     
          <TypesOfOperations />
          <Calculator />
          <Categories />
        </View>
    )
}}
